# Automation Template

## Usage

##### Possible browser options:
- Firefox
- Chrome

##### Running Tests:
- **TestNG** (running from IDE)
    - Setting:```-Dbrowser=chrome``` 
- **Gradle Wrapper** - command in Terminal/CMD:
    - All tests ```./gradlew test```(Mac) and ```gradlew test```(Win)
    - Specific ***.xml** config ```./gradlew testRegression```
        - With browser setting: ```./gradlew -Pbrowser=chrome testRegression```

## Main Tech Stack

- Java 8
- Gradle
- Selenium
- TestNG
- AssertJ
- Awaitility
- Logback
