package domain;

import java.util.Map;

import static com.google.common.collect.ImmutableMap.of;
import static domain.Language.ENGLISH;
import static domain.Language.ESTONIAN;
import static domain.Language.LATVIAN;
import static utils.ConfigurationUti.getLanguage;

public enum Translation {

    CUSTOMER(of(ENGLISH, "CUSTOMER", ESTONIAN, "KLIENT", LATVIAN, "KLIENTS"));


    private Map<String, String> translationByLanguage;

    Translation(Map<String, String> translationByLanguage) {
        this.translationByLanguage = translationByLanguage;
    }

    public String getValue() {
        return this.translationByLanguage.get(getLanguage());
    }

}
