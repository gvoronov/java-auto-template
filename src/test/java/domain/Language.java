package domain;

public interface Language {

    String ENGLISH = "en";
    String ESTONIAN = "ee";
    String LATVIAN = "lv";

}
