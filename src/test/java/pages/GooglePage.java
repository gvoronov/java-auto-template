package pages;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Slf4j
public class GooglePage extends BasePage {

    public GooglePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "input[name='q']")
    private WebElement searchInput;

    @FindBy(css = ".FPdoLc.VlcLAe input[name='btnK']")
    private WebElement searchBtn;

    public GooglePage fillSearchInput(String value) {
        log.info("Filling Search Input Field with: {}", value);
        searchInput.clear();
        searchInput.sendKeys(value);
        return this;
    }

    public SearchResultsPage clickSearchBtn() {
        log.info("Clicking on 'Google Search' button");
        searchBtn.click();
        return new SearchResultsPage(driver);
    }

}
