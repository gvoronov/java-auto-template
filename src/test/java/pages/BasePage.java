package pages;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;

@Slf4j
public abstract class BasePage extends BaseHelper {

    public BasePage(WebDriver driver) {
        super(driver);
    }

}
