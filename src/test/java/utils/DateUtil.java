package utils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;

public class DateUtil {

    public static String convertDate(String strDate, String formatFrom, String formatTo) {
        DateTimeFormatter f = new DateTimeFormatterBuilder().appendPattern(formatFrom).toFormatter();
        LocalDate parsedDate = LocalDate.parse(strDate, f);
        DateTimeFormatter f2 = DateTimeFormatter.ofPattern(formatTo);
        return parsedDate.format(f2);
    }

    public static String todayDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.YYYY");
        Date date = new Date();
        return dateFormat.format(date);
    }

}
