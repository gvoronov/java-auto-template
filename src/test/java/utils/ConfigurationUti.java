package utils;

import static domain.Language.ENGLISH;
import static org.openqa.selenium.remote.BrowserType.FIREFOX;

public class ConfigurationUti {

    public static final String DEFAULT_BROWSER = FIREFOX;
    public static final String DEFAULT_LANGUAGE = ENGLISH;

    public static String getBrowser() {
        return System.getProperty("browser", DEFAULT_BROWSER);
    }

    public static String getLanguage() {
        return System.getProperty("language", DEFAULT_LANGUAGE);
    }

}
