package tests;

import domain.Constant;
import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import static com.google.common.collect.ImmutableMap.of;
import static org.openqa.selenium.remote.BrowserType.CHROME;
import static org.openqa.selenium.remote.BrowserType.FIREFOX;
import static utils.ConfigurationUti.getBrowser;

@Slf4j
public class BaseTest {

    private static Map<String, Supplier<WebDriverManager>> webDriverManagerByBrowserName = of(
            CHROME, WebDriverManager::chromedriver,
            FIREFOX, WebDriverManager::firefoxdriver
    );

    private static Map<String, Supplier<WebDriver>> webDriverByBrowserName = of(
            CHROME, ChromeDriver::new,
            FIREFOX, FirefoxDriver::new
    );

    protected WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        String browserName = getBrowser();
        webDriverManagerByBrowserName.get(browserName)
                .get()
                .setup();

        driver = webDriverByBrowserName.get(browserName).get();
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(Constant.BASE_URL);
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        driver.quit();
    }

    @BeforeMethod
    public void beforeMethod(Method method) {
        log.info("Start test: {}", method.getName());
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(Method method) {
        log.info("End test: {}", method.getName());
    }

}
