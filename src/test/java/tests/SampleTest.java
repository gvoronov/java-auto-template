package tests;

import org.testng.annotations.Test;
import pages.GooglePage;
import pages.SearchResultsPage;

import static org.assertj.core.api.Assertions.assertThat;

public class SampleTest extends BaseTest {

    @Test(description = "Open Google page, Search for test, Verify the search results are not empty")
    public void searchGoogleTest() {
        SearchResultsPage searchResultsPage = new GooglePage(driver)
                .fillSearchInput("test")
                .clickSearchBtn();

        assertThat(searchResultsPage.getResults())
                .isNotEmpty();
    }

}
